var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.sass('home.scss', 'public/css/home.css');
    mix.version('public/css/home.css');
});

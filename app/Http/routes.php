<?php

Route::group(['domain' => 'www.' . getenv('SITE_DOMAIN')], function () {
    Route::get('/', function () {
        return redirect()->to('http://livonair.com');
    });
});

Route::group(['domain' => '{username}' . getenv('SITE_DOMAIN')], function () {
    Route::get('feed', 'FeedController@index');
    // Authentication routes...
    Route::get('login', 'Auth\AuthController@getLogin');
    Route::post('login', 'Auth\AuthController@postLogin');
    Route::get('logout', 'Auth\AuthController@getLogout');

    // Registration routes...
    Route::get('register', 'Auth\AuthController@getRegister');
    Route::post('register', 'Auth\AuthController@postRegister');

    Route::get('/', 'SubBlogController@index');
    Route::get('/post/{slug}/', 'SubBlogController@readPost');
    Route::get('/post/{slug}/edit', 'SubBlogController@editPost');
    Route::post('/post/{slug}/edit', 'SubBlogController@updatePost');
    Route::get('/post/{slug}/delete', 'SubBlogController@deletePost');

    Route::get('live', 'LiveBlogController@live');
    Route::get('live/{post}', 'LiveBlogController@blog');
    Route::post('live/{post}', 'LiveBlogController@prepend');
    Route::get('live/{post}/json', 'LiveBlogController@jsonPost');

    Route::get('search', 'BlogPostController@search');

    //Admin Routes
    Route::group(['middleware' => 'auth'], function () {
        Route::post('liveblog', 'LiveBlogController@create');
        Route::get('/api/json/analytics', 'SiloController@analytics');
        Route::get('admin', 'AdminController@index');
        Route::get('blogs', 'BlogController@showUserBlogs');
        Route::post('admin', ['uses' => 'AdminController@basic', 'as' => 'admin']);
        Route::get('admin/new', 'SubBlogController@newPost');
        Route::post('admin/new', 'SubBlogController@createPost');
        Route::get("admin/edit", 'SiloController@admin');
        Route::post("admin/edit", 'SiloController@custom');
        Route::get('admin/import', 'AdminController@getImport');
        Route::post('admin/import', 'AdminController@import');
        Route::get('blog-down', 'AdminController@down');
    });

    Route::get('/tag/{cat}', 'BlogPostController@category');

});

// API Needs to be done.

Route::get('/', function () {
    return view('homepage');
});

// Authentication routes...
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', 'Auth\AuthController@postRegister');

Route::group(['middleware' => 'auth'], function () {
    Route::get('blogs', 'BlogController@showUserBlogs');
    Route::get('{blogname}/delete', 'BlogController@deleteBlog');
    Route::get('notifications', 'BlogController@notifications');
    Route::get('newsilo', 'SiloController@index');
    Route::post('newsilo', 'SiloController@create');
    Route::get('user/edit', 'ProfileController@profileUpdatePage');
    Route::post('user/edit', 'ProfileController@updateProfile');
});

<?php

namespace App\Http\Controllers;

use App\BlogPost;
use App\Http\Controllers\Controller;
use Feed;

class FeedController extends Controller
{
    public function __construct(BlogPost $post, Feed $feed)
    {
        $this->post = $post;
        $this->feed = $feed;
    }

    public function index($username)
    {

        // cache the feed for 60 minutes
        $this->feed->setCache(60);

        // check if there is cached feed and build new only if is not
        if (!$this->feed->isCached()) {
            // creating rss feed with our most recent 20 posts
            $posts = $this->post->where('blogname', $username)->orderBy('created_at', 'desc')->take(20)->get();

            // set your feed's title, description, link, pubdate and language
            $this->feed->title = $username;
            $this->feed->description = "The feed for $username.livonair.com";
            $this->feed->link = \URL::to('feed');
            $this->feed->setDateFormat('datetime'); // 'datetime', 'timestamp' or 'carbon'
            $this->feed->lang = 'en';
            $this->feed->setShortening(true); // true or false
            $this->feed->setTextLimit(300); // maximum length of description text

            foreach ($posts as $post) {
                $this->feed->add($post->title, $username, \url('post/' . $post->slug), $post->created_at, $post->post, $post->post);
            }

        }
        return $this->feed->render('atom');

    }
}

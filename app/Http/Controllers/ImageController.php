<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Images;
use Auth;
use Request;

class ImageController extends Controller
{

    public function index()
    {
        Images::where('user_id', Auth::id())->get();
        return view('images.allimg');
    }

    public function create(Request $request)
    {
        if ($request->file('image')) {

            $image = $request->file('image');
            $input = array('image' => $image);

            $rules = array(
                'image' => 'image',
            );

            $validator = \Validator::make($input, $rules);

            if ($validator->fails()) {
                return \Redirect::back()->with('status', 'error');
            }

            $s3 = \Storage::disk('s3');
            $filePath = '/data/' . Auth::id() . md5($request->get('name'));
            $s3->put($filePath, file_get_contents($image), 'public');
        }

        $image = new Images;
        $image->user_id = Auth::id();
        $image->image_name = $request->get('name');
        $image->image_hash = md5($request->get('name'));
        $image->save();
    }

}

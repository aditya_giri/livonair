<?php namespace App\Http\Controllers;

use App\BlogInfo;
use App\BlogPost;
use App\Http\Controllers\Controller;
use App\User;

class BlogPostController extends Controller
{

    public function __construct(BlogInfo $blog, BlogPost $post, User $user)
    {
        $this->blog = $blog;
        $this->post = $post;
        $this->user = $user;
    }

    public function category($username, $cat)
    {
        $category = $cat;
        $posts = $this->post->where('blogname', $username)->where('category', 'LIKE', "%$cat%")->get();
        $user_id = $this->blog->where('blogname', $username)->first();
        $user = $this->user->find($user_id->user_id);
        return view('blog.category', compact('posts', 'user', 'category'));
    }

    public function search($username)
    {
        $blog = $this->blog->where('blogname', $username)->first();
        $result = $this->post->where('blogname', $username)->where('title', 'LIKE', '%' . \Input::get('query') . '%')->orWhere('post', 'LIKE', '%' . \Input::get('query') . '%')->get();
        return view('blog.search', compact('result', 'blog'));
    }

}

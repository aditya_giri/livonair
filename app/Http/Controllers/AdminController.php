<?php namespace App\Http\Controllers;

use App\BlogInfo;
use App\BlogPost;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    public function __construct(BlogInfo $blog, User $user, BlogPost $posts)
    {
        $this->posts = $posts;
        $this->blog = $blog;
        $this->user = $user;
    }

    public function index($username)
    {
        $blog = $this->blog->where('blogname', $username)->first();
        $user = $this->user->find($blog->user_id)->first();
        return view('admin.home', compact('blog', 'user'));
    }

    public function basic(Request $request, $username)
    {
        $blog = $this->blog->where('blogname', $username)->first();
        BlogInfo::where('blogname', $username)->update(['bloginfo' => $request->get('tagline'), 'name' => $request->get('name')]);
        return back()->withInput();
    }

    public function getImport()
    {
        return view('silo.import');
    }

    public function import($username, Request $request)
    {
        $result = simplexml_load_file($request->url);
        $json = json_encode($result, true);
        $array = json_decode($json, true);
        foreach ($array['channel']['item'] as $post) {
            $this->posts->create([
                'user_id' => auth()->id(),
                'blogname' => $username,
                'title' => $post['title'],
                'post' => $post['description'],
                'created_at' => $post['pubDate'],
                'slug' => str_slug($post['title']),
                'header_image_hash' => "none",
            ]);
        }
        return redirect()->to('/');
    }

    public function down($username)
    {
        $blog = $this->blog->where('blogname', $username)->first();
        if ($blog->blog_down == 0) {
            $blog->blog_down = 1;
            $blog->update();
        } else {
            $blog->blog_down = 0;
            $blog->update();
        }
        return redirect()->back();
    }

}

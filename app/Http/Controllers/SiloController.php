<?php namespace App\Http\Controllers;

use App\Analytics;
use App\BlogInfo;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Auth;
use DB;
use Illuminate\Http\Request;
use Storage;
use Validator;

class SiloController extends Controller
{

    public function __construct(BlogInfo $blog, Analytics $analytics)
    {
        $this->blog = $blog;
        $this->analytics = $analytics;
    }

    public function index()
    {
        return view('silo.form');
    }

    /**
     * Create new blog
     * @param  Request $request Requests all the data
     * @return Redirect         last location
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), ['name' => 'required|max:255', 'blogname' => 'required|max:50|unique:blog_info', 'bloginfo' => 'required|min:6']);

        if ($validator->fails()) {
            return redirect('newsilo')->withErrors($validator)->withInput();
        }

        $blogname = strtolower($request->get('blogname'));

        $this->blog->create([
            'name' => $request->get('name'),
            'blogname' => $blogname,
            'bloginfo' => $request->get('bloginfo'),
            'user_id' => auth()->id(),
            'image_name' => "none",
            'editor_type' => $request->get('editor_type'),
        ]);

        $array[] = ['blogname' => $blogname, 'pageviews' => '0', 'date' => date("Ymd")];
        DB::table('analytics')->insert($array);

        return redirect()->to('http://' . $request->get('blogname') . env('SITE_DOMAIN', '.livonair.com'));
    }

    public function admin($username)
    {
        $siloinfo = $this->blog->where("blogname", $username)->first();
        return view('silo.admin', compact('siloinfo'));
    }

    public function custom($username, Request $request)
    {
        $blog = $this->blog->where('blogname', $username)->first();
        $blog->analytics = $request->get('js');
        $blog->copyright = $request->get('copyright');
        $blog->fb = $request->get('fb');
        $blog->twitter = $request->get('twitter');
        $blog->tumblr = $request->get('tumblr');
        $blog->github = $request->get('github');
        $blog->website = $request->get('website');
        $blog->email = $request->get('email');
        $blog->telephone = $request->get('mobile');
        $blog->editor_type = $request->get('editor_type');
        $blog->menu_type = $request->get('menu');

        if ($request->file('image')) {
            $blog->image_name = urlencode($request->file('image')->getClientOriginalName()) ? urlencode($request->file('image')->getClientOriginalName()) : "none";
            $image = $request->file('image');
            $input = array('image' => $image);

            $rules = array(
                'image' => 'image',
            );

            $validator = \Validator::make($input, $rules);

            if ($validator->fails()) {
                return redirect()->back()->with('status', 'error');
            }

            $s3 = Storage::disk('s3');
            $filePath = '/blog-profiles/' . urlencode($request->file('image')->getClientOriginalName());
            $s3->put($filePath, file_get_contents($image), 'public');
        }

        $blog->update();
        return redirect()->back()->with('status', 'info');
    }

    public function analytics($username)
    {
        $info = $this->analytics->where('blogname', $username)->get(['pageviews', 'date']);
        return response()->json($info);
    }
}

<?php namespace App\Http\Controllers;

use App\BlogInfo;
use App\BlogPost;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{

    public function __construct(BlogInfo $blog, BlogPost $post)
    {
        $this->blog = $blog;
        $this->post = $post;
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $blogs = $this->blog->all();
        return view('allblogs', compact('blogs'));
    }

    public function showUserBlogs()
    {
        $blogs = $this->blog->where('user_id', \Auth::id())->get();

        return view('user.home', compact('blogs'));
    }

    public function deleteBlog($blogname)
    {
        $this->blog->where('blogname', $blogname)->delete();
        $this->post->where('blogname', $blogname)->delete();
        return redirect()->back();
    }

}

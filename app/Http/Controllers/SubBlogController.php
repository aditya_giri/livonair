<?php namespace App\Http\Controllers;

use App\Analytics;
use App\BlogInfo;
use App\BlogPost;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SubBlogController extends Controller
{

    public function __construct(User $user, BlogInfo $blog, BlogPost $posts, Analytics $analytics)
    {
        $this->user = $user;
        $this->blog = $blog;
        $this->post = $posts;
        $this->analytics = $analytics;
    }

    public function index($username)
    {
        // Get blog info
        $bloginfo = $this->blog->where('blogname', $username)->first();
        if (auth()->id() !== $bloginfo->user_id && $bloginfo->blog_down == 1) {
            return view('errors.503');
        }

        //Get posts
        $posts = $this->post->where('blogname', $username)->orderBy('created_at', 'desc')->paginate(10);
        //Get admin info
        $user = $this->user->find($bloginfo['user_id']);

        if (\Auth::id() !== $user->id) {
            // Analytics
            $date = new \Carbon\Carbon();
            $analytics = $this->analytics->where('blogname', $username)->where('date', date("Ymd"))->first();
            $analytics->pageviews = $analytics->pageviews + 1;
            $analytics->update();
        }

        return view('user.default', compact('posts', 'bloginfo', 'user'));
    }

    public function newPost($username)
    {
        if (session()->has('edit')) {
            session()->forget('edit');
        }
        if (\Auth::guest()) {return redirect('/login');}
        $bloginfo = $this->blog->where('blogname', $username)->first();
        if (\Auth::id() !== $bloginfo['user_id']) {return \Redirect::to('/');}
        return view('user.newpost', compact('bloginfo'));
    }

    public function createPost(Request $request, $username)
    {
        $imageName = str_random(100);
        $slug = $this->getSlug($username, $request->title);
        $this->post->create([
            'blogname' => $username,
            'post' => $request->editor,
            'title' => $request->title,
            'slug' => $slug,
            'category' => $request->tags,
            'user_id' => \Auth::id(),
            'header_image_hash' => $request->file('image') ? $imageName : "none",
            'file_type' => $request->editor_type,
        ]);
        if ($request->file('image')) {
            $image = $request->file('image');
            $input = array('image' => $image);

            $rules = array(
                'image' => 'image',
            );

            $validator = \Validator::make($input, $rules);

            if ($validator->fails()) {
                return \Redirect::back()->with('status', 'error');
            }

            $s3 = \Storage::disk('s3');
            $filePath = '/header/' . $imageName;
            $s3->put($filePath, file_get_contents($image), 'public');
        }

        if (session()->has('edit')) {
            session()->forget('edit');
        }

        return \Redirect::to('/');
    }

    public function readPost($username, $slug)
    {

        $bloginfo = $this->blog->where('blogname', $username)->first()->toArray();
        if (auth()->id() !== $bloginfo['user_id'] && $bloginfo['blog_down'] == 1) {
            return view('errors.503');
        }
        $post = $this->post->where('blogname', $username)->where('slug', $slug)->first()->toArray();
        $user = $this->user->find($post['user_id'])->toArray();
        // Analytics
        if (\Auth::id() !== $user['id']) {
            $date = new \Carbon\Carbon();
            $analytics = $this->analytics->where('blogname', $username)->where('date', date("Ymd"))->first();
            $analytics->pageviews = $analytics->pageviews + 1;
            $analytics->update();
        }
        return view('user.read', compact('post', 'user', 'bloginfo'));
    }

    public function editPost($username, $slug)
    {
        $bloginfo = $this->blog->where('blogname', $username)->first();
        $user = $this->user->find($bloginfo->user_id);
        if (\Auth::id() === $user->id) {
            $post = $this->post->where('blogname', $username)->where('slug', $slug)->first();
            session()->put('edit', 'true');
            return view('user.newpost', compact('post', 'bloginfo'));
        }
        return \Redirect::back();
    }

    public function updatePost(Request $request, $username, $slug)
    {
        $imageName = str_random(100);
        $this->post->where('blogname', $username)->where('slug', $slug)->update([
            'blogname' => $username,
            'post' => $request->editor,
            'title' => $request->title,
            'category' => $request->tags,
            'user_id' => \Auth::id(),
            'header_image_hash' => $request->file('image') ? $imageName : "none",
        ]);
        if ($request->file('image')) {
            $image = $request->file('image');
            $input = array('image' => $image);

            $rules = array(
                'image' => 'image',
            );

            $validator = \Validator::make($input, $rules);

            if ($validator->fails()) {
                return \Redirect::back()->with('status', 'error');
            }

            $s3 = \Storage::disk('s3');
            $filePath = '/header/' . $imageName;
            $s3->put($filePath, file_get_contents($image), 'public');
        }
        if (session()->has('edit')) {
            session()->forget('edit');
        }

        return \Redirect::to('post/' . $slug);

    }

    private function getSlug($username, $title)
    {
        $slug = Str::slug($title);
        $posts = $this->post->where('slug', $slug)->first();
        if (count($posts) !== 0) {
            $slug = $slug . "-" . str_random(2);
        }
        return $slug;
    }

    /**
     * Deletes a post
     * @param  string $username Name of the blog
     * @param  string $slug     Slug for the post
     * @return Redirect           Redirects back
     */
    public function deletePost($username, $slug)
    {
        $bloginfo = $this->blog->where('blogname', $username)->first();
        $user = $this->user->find($bloginfo->user_id);
        if (\Auth::id() === $user->id) {
            $post = $this->post->where('blogname', $username)->where('slug', $slug)->first();
            $post->delete();
            return \Redirect::to('/');
        }
        return \Redirect::back();
    }

}

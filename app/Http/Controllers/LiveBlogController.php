<?php

namespace App\Http\Controllers;

use App\BlogInfo;
use App\Http\Controllers\Controller;
use App\LiveBlog;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as JavaScript;

class LiveBlogController extends Controller
{

    public function __construct(LiveBlog $livepost, BlogInfo $blog)
    {
        $this->livepost = $livepost;
        $this->blog = $blog;
    }

    public function live($username)
    {
        $list = $this->livepost->where('blogname', $username)->get();
        $blog = $this->blog->where('blogname', $username)->first();
        if (auth()->id() !== $blog->user_id && $blog->blog_down == 1) {
            return view('errors.503');
        }
        return view("live.always", compact('list'));
    }

    public function blog($username, $post)
    {
        $blog = $this->blog->where('blogname', $username)->first();
        if (auth()->id() !== $blog->user_id && $blog->blog_down == 1) {
            return view('errors.503');
        }
        $post = $this->livepost->where('blogname', $username)->where('slug', $post)->first();
        return view('live.issue', compact('post', 'blog'));
    }

    public function jsonPost($username, $post)
    {
        $data = $this->livepost->where('blogname', $username)->where('slug', $post)->first();
        return \Response::json($data->post);
    }

    public function create($username, Request $request)
    {
        $b = new LiveBlog;
        $b->title = $request->get('title');
        $b->slug = str_slug($request->get('title'), "-");
        $b->blogname = $request->get('_blogname');
        $b->user_id = Auth::id();
        $b->desc = $request->get('description');
        $b->save();
        Javascript::put([
            'loaction' => url('live/' . $b->slug),
        ]);
        return redirect('live/' . $b->slug);
    }

    public function prepend($username, $post, Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'text' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back();
        }

        $post = $this->livepost->where('blogname', $username)->where('slug', $post)->first();
        $date = date('F d Y  \a\t  h:i A', strtotime(Carbon::now()));
        $text = "<br /><div class=\"inline\"><div class=\"date\">" . $date . ":</div>&nbsp" . $request->get('text') . "</div><br />" . $post->post;
        $post->post = $text;
        $post->update();
        Javascript::put([
            'status' => 'created',
            'post' => $post->post,
        ]);
        return \Redirect::back();
    }
}

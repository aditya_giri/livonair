<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct(User $user)
    {
        $this->user = $user->find(auth()->id());
    }

    public function profileUpdatePage()
    {
        $user = $this->user;
        return view('user.edit', compact('user'));
    }

    public function updateProfile(Request $request)
    {
        $this->user->name = $request->get('name');
        $this->user->info = $request->get('info');
        $this->user->update();

        if ($request->file('image')) {

            $image = $request->file('image');
            $input = array('image' => $image);

            $rules = array(
                'image' => 'image',
            );

            $validator = \Validator::make($input, $rules);

            if ($validator->fails()) {
                return \Redirect::back()->with('status', 'error');
            }

            $s3 = \Storage::disk('s3');
            $filePath = '/author/' . $this->user->id;
            $s3->put($filePath, file_get_contents($image), 'public');
        }

        return redirect()->back()->with('updated', 'true');
    }

}

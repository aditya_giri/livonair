<?php
namespace LA\Blog;

use Redirect;
use Storage;
use Validator;

class AddImage
{
    public function image($image)
    {
        $image = $request->file('image');
        $input = array('image' => $image);

        $rules = array(
            'image' => 'image',
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::back()->with('status', 'error');
        }

        $s3 = Storage::disk('s3');
        $filePath = '/author/' . $this->user->id;
        $s3->put($filePath, file_get_contents($image), 'public');
    }
}

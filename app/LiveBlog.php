<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LiveBlog extends Model
{
    protected $table = 'liveblog';
}

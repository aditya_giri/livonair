<?php use League\CommonMark\CommonMarkConverter;
$converter = new CommonMarkConverter();?>

@extends('liveblog')

@section('title')
	Search Results | LivonAir
@stop

@section('scripts')
	<script type="text/javascript" src="{{ asset('js/favico.js') }}"></script>
@stop

@section('content')
	<div class="row">
		<div class="nine columns">
			<form action="{{ url('/search') }}">
				<input type="text" name="query" style="width:100%;">
			</form>
			@if(count($result) !== 0)
				@foreach ($result as $r)
					@if($r->file_type === 'MD')
						<?php $r->post = $converter->convertToHtml($r->post)?>
						@if (strpos($r->post, "</p>"))
							<?php $str = strpos($r->post, "</p>")?>
						@else
							<?php $str = strlen($r->post)?>
						@endif
					@else
						@if (strpos($r->post, "</p>"))
							<?php $str = strpos($r->post, "</p>")?>
						@else
							<?php $str = strlen($r->post)?>
						@endif
					@endif

					<article>
						<h2><a href="{{ url('/post/' . $r->slug) }}">{{ $r->title }}</a></h2>
						<p>{!! substr($r->post, 0, $str) !!}</p>
					</article>
				@endforeach
			@else
				<h1>Sorry, No search results were found.</h1>
			@endif
		</div>
		<div class="three columns">
			<center>
				@if($blog->image_name === "none")
					<img src="{{ asset('no-avatar.jpg') }}" class="img-responsive img-circle img-thumbnail" id="profilepic" width="100">
				@else
					<img src="https://s3-ap-southeast-1.amazonaws.com/livonair/blog-profiles/{{ $blog->image_name }}" class="img-responsive img-circle img-thumbnail" id="profilepic" width="100">
				@endif
			</center>
			<center><h1>{{ $blog->name }}</h1></center>
			<center><p>{{ $blog->bloginfo }}</p></center>

		</div>
	</div>
	<script type="text/javascript">
		var favicon=new Favico({
		    animation:'slide'
		});
		favicon.badge({{ count($result) }});
	</script>
@stop

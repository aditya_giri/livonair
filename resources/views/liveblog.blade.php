<!DOCTYPE html>
<html>
<head>
	@yield('meta')
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title')</title>
	<script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/normalize.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/liveblog.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/global.css') }}">
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
	<script src="//fast.eager.io/ioo3Am8MmR.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
	<script type="text/javascript" src="{{ asset('js/global.js') }}"></script>
	@yield('scripts')
</head>
<body>
	<!-- Menu -->
	<div class="outer-menu">
	  <input class="checkbox-toggle" type="checkbox" />
	  <div class="hamburger">
	    <div></div>
	  </div>
	  <div class="menu">
	    <div>
	      <div>
	        <ul>
	          <li><form action="{{ url('/search') }}"><input type="text" name="query" class="search-box"/></form></li>
	          <li><a href="{{ url('live') }}">Live Blogs</a></li>
	        @if(Auth::guest())
	          <li><a href="{{ url('login') }}">Login</a></li>
	          <li><a href="{{ url('register') }}">Register</a></li>
	        @else
				<li><a href="{{ url('blogs') }}">All Blogs</a></li>
				<li><a href="{{ url('admin') }}">Admin Page</a></li>
	        @endif
	        </ul>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="container">
	@yield('content')
	</div>
	@yield('scripts')
	{{-- Google Analytics Code --}}
	<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-63818132-1', 'auto');ga('send', 'pageview');</script>

</body>
</html>

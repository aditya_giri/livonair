@extends('dashboard')

@section('title')
	Images
@stop

@section('css')

	<link rel="stylesheet" type="text/css" href="{{ asset('css/images.css') }}">

@stop

@section('content')



	<button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored fab">
		<i class="material-icons">add</i>
	</button>

@stop

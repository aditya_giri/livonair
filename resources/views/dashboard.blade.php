<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | LivonAir</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('mdl/material.min.css') }}">
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}">
    <script type="text/javascript" src="{{ asset('js/global.js') }}"></script>
    <script src="//fast.eager.io/ioo3Am8MmR.js"></script>
    @yield('css')
  </head>
  <body>
    <div class="la-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <header class="la-header mdl-layout__header mdl-color--white mdl-color--grey-100 mdl-color-text--grey-600">
        <div class="mdl-layout__header-row">
          <span class="mdl-layout-title">@yield('title')</span>

        </div>
      </header>
      <div class="la-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
        <header class="la-drawer-header">
          <div class="la-avatar-dropdown">
            <span>Hi {{ \Auth::user()->name }}</span>
            <div class="mdl-layout-spacer"></div>
          </div>
        </header>
        <nav class="la-navigation mdl-navigation mdl-color--blue-grey-800">
          <a class="mdl-navigation__link" href="{{ url('blogs') }}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Home</a>
          <a class="mdl-navigation__link" href="{{ url('user/edit') }}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">account_circle</i>Author Info</a>
          <a class="mdl-navigation__link" href="{{ url('logout') }}"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">exit_to_app</i>Logout</a>
          <div class="mdl-layout-spacer"></div>
          <a class="mdl-navigation__link" href=""><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">help_outline</i><span class="visuallyhidden">Help</span></a>
        </nav>
      </div>
      <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid la-content">
          @yield('content')
        </div>
      </main>
    </div>
    <script src="{{ asset('mdl/material.min.js') }}"></script>
    @yield('scripts')
  </body>
</html>

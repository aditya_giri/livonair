@extends('app')

@section('title')
	Register
@stop

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
	<style type="text/css">
		.card {
			width: 400px;	
			margin: 0 auto;		
			margin-top: 50px;
		}
	</style>
@stop

@section('content')
<div class="mdl-card mdl-shadow--2dp card">
  <div class="mdl-card__title">
    <h2 class="mdl-card__title-text">Welcome</h2>
  </div>
  <div class="mdl-card__supporting-text">
		<form role="form" method="POST" action="{{ url('/register') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group mdl-textfield mdl-js-textfield mdl-textfield--floating-label textfield-demo">
				<label class="mdl-textfield__label" for="name">Name</label>
					<input class="mdl-textfield__input" id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">
			</div>
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label textfield-demo">
			    <input class="mdl-textfield__input" id="email" type="email" name="email" value="{{ old('email') }}"/>
			    <label class="mdl-textfield__label" for="email">Email Address</label>
			</div>

			<div class="form-group">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label textfield-demo">
				    <input class="mdl-textfield__input" type="password" id="password" name="password"/>
				    <label class="mdl-textfield__label" id="password">Password</label>
				</div>
			</div>
			<div class="form-group">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label textfield-demo">
				    <input class="mdl-textfield__input" type="password" id="password_confirmation" name="password_confirmation"/>
				    <label class="mdl-textfield__label" id="password_confirmation">Confirm Password</label>
				</div>
			</div>
			<div class="form-group">
				<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Register</button>
				<a class="mdl-button mdl-js-button mdl-js-ripple-effect" href="{{ url('/login') }}">Login</a>
			</div>
		</form>
  </div>
</div>
<script type="text/javascript" src="{{ asset('/js/sweetalert.js') }}"></script>
<script type="text/javascript">
	@if (count($errors) > 0)
		@foreach ($errors->all() as $error)
			swal("Error", "<?= $error ?>", "error");
		@endforeach
	@endif
</script>
@endsection

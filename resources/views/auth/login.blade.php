@extends('app')

@section('title')
	Login
@stop

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
	<style type="text/css">
		.card {
			width: 400px;	
			margin: 0 auto;		
			margin-top: 50px;
		}
	</style>
@stop

@section('content')
<div class="mdl-card mdl-shadow--2dp card">
  <div class="mdl-card__title">
    <h2 class="mdl-card__title-text">Welcome</h2>
  </div>
  <div class="mdl-card__supporting-text">
		<form role="form" method="POST" action="{{ url('/login') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label textfield-demo">
				    <input class="mdl-textfield__input" id="email" type="email" name="email" value="{{ old('email') }}"/>
				    <label class="mdl-textfield__label" for="email">Email Address</label>
				</div>
			</div>

			<div class="form-group">
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label textfield-demo">
				    <input class="mdl-textfield__input" type="password" id="password" name="password"/>
				    <label class="mdl-textfield__label" id="password">Password</label>
				</div>
			</div>
			<div id="check-awesome" class="form-group">   
			  	<label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="switch">
				  <input type="checkbox" id="switch" name="remember" class="mdl-switch__input" />
				  <span class="mdl-switch__label">Remember Me.</span>
				</label>
			</div>
			<hr/>
			<div class="form-group">
				<div class="col-md-6 col-md-offset-4">
					<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Login</button>
					<a class="mdl-button mdl-js-button mdl-js-ripple-effect" href="{{ url('/register') }}">Register</a>
				</div>
			</div>
		</form>
  </div>
</div>
<script type="text/javascript" src="{{ asset('/js/sweetalert.js') }}"></script>
<script type="text/javascript">
	@if (count($errors) > 0)
		@foreach ($errors->all() as $error)
			swal("Error", "<?= $error ?>", "error");
		@endforeach
	@endif
</script>

@endsection

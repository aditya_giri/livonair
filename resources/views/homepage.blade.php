<html>
	<head>
		<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="{{ asset('favicons/ms-icon-144x144.png') }}">
		<meta name="theme-color" content="#ffffff">
		<title>LivonAir - Blogging++</title>
		<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #2c3e50;
				background-color: #ecf0f1;
				display: table;
				font-weight: 500;
				font-family: 'Lato';
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 72px;
				margin-bottom: 40px;
			}
			.low {
				font-size: 36px;
			}
			.banner {
				width: 300px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<center><img class="banner" src="{{ asset('icon.png') }}"></img></center>
				<div class="title">Coming Soon</div>
				<div class="low">We are getting a little makeover.</div>
			</div>
		</div>
	</body>
</html>

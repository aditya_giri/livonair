@if (Auth::guest() || Auth::id() !== $blog->user_id)
	<?php return \Redirect::to(url('/'))?>
@endif
<!DOCTYPE html>
<html>
<head>
	<title>Admin | LivOnAir</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/darkly/bootstrap.min.css">
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
	<script src="//fast.eager.io/ioo3Am8MmR.js"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/admin.css') }}">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="well well-lg box col-md-3">
				<h2>{{ $blog->name }}</h2><br/>
				<p>{{ $blog->bloginfo }}</p>
				<a class="btn btn-primary" data-toggle="modal" href='#userinfo'>Edit This Info</a>
			</div>
			<div class="well well-lg box col-md-3">
				<a class="btn btn-primary" href="{{ url('admin/new') }}">New Post</a>
			</div>
			<div class="well well-lg box col-md-3">
				Want to edit even further?<br/>
				<a href="{{ url('admin/edit') }}" class="btn btn-primary">Edit Now</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-5 box well well-lg">
				<a href="{{ url('admin/import') }}" class="btn btn-warning">Import Data</a>
			</div>
			<div class="box well well-lg col-md-5">
				<a class="btn btn-primary" data-toggle="modal" href='#liveblog'>Start a live blog</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-11 box well-lg well">
				@if($blog->blog_down == 0)
					<a class="btn btn-danger" href="{{ url('blog-down') }}">Make Blog Down For Some Reason</a>
				@else
					<a class="btn btn-warning" href="{{ url('blog-down') }}">Make Blog Up And Running Again</a>
				@endif
			</div>
		</div>
		<canvas id="myChart" style="width: 100%; height: 400px;"></canvas>
	</div>

	<div class="modal fade" id="liveblog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Live Blog</h4>
				</div>
				<div class="modal-body">
					<form method="POST" action="{{ url('liveblog') }}" accept-charset="UTF-8" data-liveblog>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="_blogname" value="{{ $blog->blogname }}">
					    <div class="form-group">
					        <label for="title">Title</label>
					        <input class="form-control" required="required" name="title" type="text" id="title">
					    </div>
					    <div class="form-group">
					        <label for="description">Description</label>
					        <input class="form-control" required="required" name="description" type="text" id="description">
					    </div>
						<div class="alert alert-warning">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							<strong>Note</strong> Once added, things cannot be removed or added
						</div>
						<div class="modal-footer">
							<div class="btn-group pull-right">
								<input class="btn btn-warning" type="reset" value="Reset">
								<input class="btn btn-success" type="submit" value="Add">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="userinfo">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Edit User Info</h4>
				</div>
				<div class="modal-body">
					<form method="POST" action="{{ url('admin') }}" accept-charset="UTF-8" data-remote>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					    <div class="form-group">
					        <label for="name">Name</label>
					        <input class="form-control" required="required" value="{{ $blog->name }}" name="name" type="text" id="name">
					    </div>

					    <div class="form-group">
					        <label for="tagline">Tagline</label>
					        <input class="form-control" required="required" name="tagline" type="text" value="{{ $blog->bloginfo }}" id="tagline">
					    </div>
						<div class="modal-footer">
							<div class="btn-group pull-right">
								<input class="btn btn-warning" type="reset" value="Reset">
								<input class="btn btn-success" type="submit" value="Add">
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
	<script type="text/javascript" src="{{ asset('js/admin.js') }}"></script>
	<script type="text/javascript">
		var msg = $.ajax({type: "GET", url: "api/json/analytics", async: false}).responseText;
		var ctx = $("#myChart").get(0).getContext("2d");
		var dates = new Array();
	    var val = new Array();

		var obj = jQuery.parseJSON(msg);
		$.each(obj, function(key,value) {
		  dates.push(value.date);
		  val.push(value.pageviews);
		});
		Chart.defaults.global.responsive = true;
		var myLineChart = new Chart(ctx).Line({
		    labels: dates,
		    datasets: [
		        {
		            label: "PageLoads",
		            fillColor: "rgba(220,220,220,0.2)",
		            strokeColor: "rgba(220,220,220,1)",
		            pointColor: "rgba(220,220,220,1)",
		            pointStrokeColor: "#fff",
		            pointHighlightFill: "#fff",
		            pointHighlightStroke: "rgba(220,220,220,1)",
		            data: val
		        }
		    ]
		});
	</script>
</body>
</html>
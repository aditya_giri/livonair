@extends('liveblog')

@section('title')
	All Live Blogs
@stop

@section('content')
	<div class="list">
		@if(count($list) !== 0)
		@foreach ($list as $lb)
			<article>
				<h2><a href="{{ url('/live/' . $lb->slug) }}">Live Blog: {{ $lb->title }}</a></h2>
				<p>{{ $lb->desc }}</p>
			</article>
		@endforeach
		@else
			Sorry, No live blogs found.
		@endif
	</div>
@stop

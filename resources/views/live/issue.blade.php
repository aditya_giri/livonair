@extends('liveblog')

@section('title')
	{{ $post->title }}
@stop

@section('meta')
	<meta http-equiv="refresh" content="60">
@stop


@section('content')
<div class="row">

	<div class="nine columns">
	@if(Auth::id() === $post->user_id)
		<center>
			<form method="POST" data-remote>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<textarea id="editor" name="text" required></textarea>
				<input type="submit" class="btn btn-submit btn-submit-a">
			</form>
		</center>
		<script type="text/javascript" src="{{ asset('js/liveblog.js') }}"></script>
		<script src="{{ asset('editor/ckeditor.js') }}"></script>
		<script>
			CKEDITOR.replace( 'editor' );
		</script>
	@endif
	<div id="post">
		{!! $post->post !!}
	</div>
</div>
	<div class="three columns seperate">
	<center>
		@if($blog->image_name === "none")
			<img src="{{ asset('no-avatar.jpg') }}" class="img-responsive img-circle img-thumbnail" id="profilepic" width="100">
		@else
			<img src="https://s3-ap-southeast-1.amazonaws.com/livonair/blog-profiles/{{ $blog->image_name }}" class="img-responsive img-circle img-thumbnail" id="profilepic" width="100">
		@endif
	</center>
	<center><h1>{{ $blog->name }}</h1></center>
	<center><p>{{ $blog->bloginfo }}</p></center>
	<hr/>
	<center><h1>{{ $post->title }}</h1></center>
	<center><p>{{ $post->desc }}</p></center>
	</div>
</div>
@stop

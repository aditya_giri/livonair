<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="description" content="LivOnAir is a blogging platform which is proudly made in India with a lots of ♥♥♥">
  <meta name="keywords" content="livonair, twitter, livonair.com, livonair twitter, livonair social network, livonair aditya, aditya, aditya giri">
  @yield('meta')
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<script src="//fast.eager.io/ioo3Am8MmR.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title') | LivOnAir</title>
  	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
  	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
  	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.4/material.indigo-pink.min.css">
  <script src="//storage.googleapis.com/code.getmdl.io/1.0.0/material.min.js"></script>
  <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/global.css') }}">
  <script type="text/javascript" src="{{ asset('js/global.js') }}"></script>
	@yield('css')
</head>
<body>
	<div class="mdl-layout">
	  <main class="mdl-layout__content">
		@yield('content')
	  </main>
	</div>
	<!-- Scripts -->
	@yield('script')
	{{-- Google Analytics Code --}}
	<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-63818132-1', 'auto');ga('send', 'pageview');</script>
</body>
</html>

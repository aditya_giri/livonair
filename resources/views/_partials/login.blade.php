@if(\Session::has('login'))
@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('/css/sweetalert.css') }}">
	<script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
@stop
@section('script')
	<script type="text/javascript">
		swal("Logged In", "You are successfully authenticated", "success")
	</script>
@stop
@endif
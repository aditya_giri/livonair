@extends('dashboard')

@section('title')
	Blogs
@stop

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
@stop

@section('content')
	@if (count($blogs) !== 0)
		@foreach ($blogs as $blog)
			<div class="mdl-card mdl-shadow--2dp blog-card">
			  <div class="mdl-card__title">
			    <h2 class="mdl-card__title-text">{{ $blog->name }}</h2>
			  </div>
			  <div class="mdl-card__supporting-text">
			    {{ $blog->bloginfo }}
			  </div>
			  <div class="mdl-card__actions mdl-card--border">
			    <a href="http://{{ $blog->blogname }}.livonair.com/admin/" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
			      Admin Page
			    </a>
			    <button class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored">
				  <a href="{{ url($blog->blogname . '/delete') }}" class="material-icons">delete</a>
				</button>
			  </div>
			</div>
		@endforeach
		<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect blog-card" href="{{ url('/newsilo') }}">
			Create New Blog
		</a>
	@else
		<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect blog-card" href="{{ url('/newsilo') }}">
			Create New Blog
		</a>
	@endif

	@if(\Session::has('registered'))
		<script type="text/javascript" src="{{ asset('/js/sweetalert.js') }}"></script>
		<script type="text/javascript">swal("Success", "You are registered. You are now free to create blog.", "success");</script>
	@endif

@stop
@include('_partials/login')

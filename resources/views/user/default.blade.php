<?php use League\CommonMark\CommonMarkConverter;
$converter = new CommonMarkConverter();?>
<!DOCTYPE html>
<html>
<head>

	<title>{{ $bloginfo['name'] }} | LivOnAir</title>
	<script src="//fast.eager.io/ioo3Am8MmR.js"></script>
	<meta charset="utf-8" />
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="{{ $bloginfo['bloginfo'] }}">
	<meta property="og:title" content="{{ $bloginfo['name'] }} by {{ $user['name'] }} on LivOnAir" />
	<meta property="og:site_name" content="{{ $bloginfo['bloginfo'] }} on LivOnAir" />
	<meta property="og:url" content="http://{{ $bloginfo['blogname'] }}.livonair.com" />
	<meta property="og:description" content="{{$bloginfo['bloginfo']}}" />
	<meta property="og:image" content="https://s3-ap-southeast-1.amazonaws.com/livonair/blog-profiles/{{ $bloginfo['image_name'] }}" />
	<meta name="twitter:card" content="summary">
	<meta name="twitter:url" content="http://{{ $bloginfo['blogname'] }}.livonair.com" >
	<meta name="twitter:title" content="{{ $bloginfo['name'] }} by {{ $user['name'] }} on LivOnAir">
	<meta name="twitter:description" content="{{$bloginfo['bloginfo']}}">
	<meta name="twitter:image" content="https://s3-ap-southeast-1.amazonaws.com/livonair/blog-profiles/{{ $bloginfo['image_name'] }}" >
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.2/material.red-green.min.css" />
	<script src="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.min.js"></script>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/blog-new.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/global.css') }}">
	<script type="text/javascript" src="{{ asset('js/global.js') }}"></script>
	<script type='text/javascript' src='//eclkmpsa.com/adServe/banners?tid=59140_91736_0&tagid=2'></script>
	<script type='text/javascript' src='//eclkmpbn.com/adServe/banners?tid=59140_91736_3&type=shadowbox&size=800x440'></script>
</head>

<body>


    <nav id="menu" class="menu">
      <a href="http://livonair.com" target="_blank">
        <header class="menu-header">
          <span class="menu-header-title">Powered By LivonAir</span>
        </header>
      </a>

      <section class="menu-section">
        <h3 class="menu-section-title">About Blog</h3>
        <ul class="menu-section-list">
        	<li><a href="http://{{ $bloginfo['blogname'] }}.livonair.com">{{ $bloginfo['name'] }}</a></li>
          <li><a href="{{ url('live') }}" target="_blank">Live Blogs</a></li>
        </ul>
      </section>
	  <section class="menu-section">
        <h3 class="menu-section-title">LivonAir</h3>
        <ul class="menu-section-list">
        	@if(Auth::guest())
	          <li><a href="{{ url('login') }}" target="_blank">Login</a></li>
	          <li><a href="{{ url('register') }}" target="_blank">Register</a></li>
	        @else
			  <li><a href="{{ url('blogs') }}" target="_blank">My Blogs</a></li>
	        @endif

        </ul>
      </section>
    </nav>
<div id="panel">
	<a href="#" class="toggle-button menu-btn {{ $bloginfo['menu_type'] }}"><span></span></a>

	<div class="mdl-layout">
	<main class="mdl-layout__content">
	    <div class="page-content">

	<div class="container">
		@if( ! isset($_GET['page']) )
			<div class="user-info">
				@if($bloginfo['image_name'] === "none")
					<img src="{{ asset('no-avatar.jpg') }}" class="img-responsive img-circle img-thumbnail" id="profilepic" width="100">
				@else
					<img src="https://s3-ap-southeast-1.amazonaws.com/livonair/blog-profiles/{{ $bloginfo['image_name'] }}" class="img-responsive img-circle img-thumbnail" id="profilepic" width="100">
				@endif
				<h1>{{ $bloginfo['name'] }}</h1>
				<p>{{ $bloginfo['bloginfo'] }}</p>
			</div>
		@endif
		<div class="user-posts">
			@if (count($posts) === 0)
				<article>
					<h2>No Articles Found</h2>
					<p>Sorry no blog posts found</p>
				</article>
			@else

				@foreach ($posts as $post)
					@if($post['file_type'] === 'MD')
						<?php $post['post'] = $converter->convertToHtml($post['post'])?>
						@if (strpos($post['post'], "</p>"))
							<?php $str = strpos($post['post'], "</p>")?>
						@else
							<?php $str = strlen($post['post'])?>
						@endif
					@else
						@if (strpos($post['post'], "</p>"))
							<?php $str = strpos($post['post'], "</p>")?>
						@else
							<?php $str = strlen($post['post'])?>
						@endif
					@endif
					<article class="epost">
						@if($post['header_image_hash'] !== "none")
							<a href="{{ url('post/' . $post['slug']) }}"><img src="https://s3-ap-southeast-1.amazonaws.com/livonair/header/{{ $post['header_image_hash'] }}" class="responsive img-thumbnail"></a>
						@endif
						<h2 class="head"><a href="{{ url('post/' . $post['slug']) }}">{{ $post['title'] }}</a></h2>
						<time>{{ date('F d, Y', strtotime($post['created_at'])) }}</time>
						<div class="post"><p>{!! substr($post['post'], 0, $str) !!}</p></div>
					</article>
				@endforeach
				<center>
				{!! $posts->render() !!}
				</center>
			@endif
		</div>
		@if(\Auth::id() === $bloginfo['user_id'])
		<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect fab" href="{{ url('admin/new') }}">
		  <i class="material-icons">add</i>
		</a>
		<a class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect fab-admin" href="{{ url('admin') }}">
		  <i class="material-icons">account_circle</i>
		</a>
		@endif
<!-- Advertises of blogger. -->
		@if(isset($bloginfo['analytics']))
				{!! $bloginfo['analytics'] !!}
		@endif
	</div>
	</div>
			<footer class="mdl-mini-footer">
		  <div class="mdl-mini-footer--left-section">
		    <ul class="mdl-mini-footer--link-list">
		      <li>&copy; {{ $bloginfo['copyright'] or $bloginfo['name'] }}</li>
				@if(isset($bloginfo['fb']))
					<li><a class="fa fa-facebook-official" href="http://fb.com/{{ $bloginfo['fb'] }}"></a></li>
				@endif
				@if(isset($bloginfo['twitter']))
					<li><a class="fa fa-twitter" href="http://twitter.com/{{ $bloginfo['twitter'] }}"></a></li>
				@endif
				@if(isset($bloginfo['github']))
					<li><a class="fa fa-github" href="http://github.com/{{ $bloginfo['github'] }}"></a></li>
				@endif
				@if(isset($bloginfo['linkedin']))
					<li><a class="fa fa-linkedin" href="http://linkedin.com/{{ $bloginfo['linkedin'] }}"></a></li>
				@endif
				@if(isset($bloginfo['tumblr']))
					<li><a class="fa fa-tumblr" href="http://{{ $bloginfo['fb'] }}.tumblr.com"></a></li>
				@endif
				@if(isset($bloginfo['website']))
					<li><a class="fa fa-connectdevelop" href="http://{{ $bloginfo['website'] }}"></a></li>
				@endif
				@if(isset($bloginfo['email']))
					<li><a class="fa fa-envelope-o" href="mailto:{{ $bloginfo['email'] }}"></a></li>
				@endif
				@if(isset($bloginfo['telephone']))
					<li><a class="fa"> <i class="fa fa-phone"></i> {{ $bloginfo['telephone'] }} </a></li>
				@endif
				<li><a href="{{ url('feed') }}" class="fa fa-rss"></a></li>
		      <li><a href="http://livonair.com">LivOnAir</a></li>
		    </ul>
		  </div>
		</footer>
	  </main>
	  </div>
	  </div>
{{-- Google Analytics Code --}}
	<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-63818132-1', 'auto');ga('send', 'pageview');</script>
	<script type="text/javascript">
		var slideout = new Slideout({
			'panel': document.getElementById("panel"),
			'menu': document.getElementById("menu"),
			'padding': 256,
			'tolerance': 70
		});

		document.querySelector('.toggle-button').addEventListener('click', function() {
	    	slideout.toggle();
	    });
	</script>
</body>
</html>
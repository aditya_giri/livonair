@extends('dashboard')

@section('title')
	Edit User Info
@stop

@section('css')
	<style type="text/css">
	.add-margin {
		margin: 10px;
	}
	</style>
	<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
	<script src="//fast.eager.io/ioo3Am8MmR.js"></script>
	<link rel="stylesheet" type="text/css" href="http://getskeleton.com/dist/css/skeleton.css">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/dashboard.css') }}">
	@if(session()->has('updated'))
		<link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
	@endif
@stop

@section('content')
	<div class="mdl-card mdl-shadow--2dp add-margin u-full-width">
		<div class="mdl-card__title">
			<h2 class="mdl-card__title-text">Update Info</h2>
		</div>

		<div class="mdl-card__supporting-text">
			<form method="post" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<label for="name">Name: </label>
				<input type="text" value="{{ $user->name }}" id="name" name="name" class="u-full-width">

				<label for="info">Your Info</label>
				<textarea id="info" name="info" class="u-full-width">{{ $user->info }}</textarea>

				<label for="image">Your Image</label>
				<input type="file" accept="image/*" id="image" name="image">
				(Note: Recommended size is 300x200)

				<input type="submit" class="button button-primary">

			</form>
		</div>
	</div>

	@if(session()->has('updated'))
		<script type="text/javascript" src="{{ asset('js/sweetalert.js') }}"></script>
		<script type="text/javascript">swal("Success", "Updated Successfully!", "success")</script>
	@endif

@stop

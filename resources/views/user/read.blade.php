<?php use League\CommonMark\CommonMarkConverter;
$converter = new CommonMarkConverter();?>

<?php $article = \App\BlogPost::where('id', $post['id'])->first()?>
<?php function getUrl()
{
    $url = @($_SERVER["HTTPS"] != 'on') ? 'http://' . $_SERVER["SERVER_NAME"] : 'https://' . $_SERVER["SERVER_NAME"];
    $url .= ($_SERVER["SERVER_PORT"] !== 80) ? ":" . $_SERVER["SERVER_PORT"] : "";
    $url .= $_SERVER["REQUEST_URI"];
    return $url;
}?>
<!DOCTYPE html>
<html>
<head>
	<title>{{ $post['title'] }} | LivOnAir</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta property="og:title" content="{{ $post['title'] }}" />
	<meta property="og:site_name" content="LivOnAir" />
	<meta property="og:type" content="article" />
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
	@if($post['header_image_hash'] !== "none")
		<meta property="og:image" content="https://s3-ap-southeast-1.amazonaws.com/livonair/header/{{ $post['header_image_hash'] }}" />
	@endif
	<meta property="og:url" content="http://{{ $post['blogname'] }}.livonair.com/" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="@LivonairApp" />
	<meta name="twitter:title" content="{{ $post['title'] }}" />
	<script src="//fast.eager.io/ioo3Am8MmR.js"></script>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/sweetalert.css') }}">
	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.yellow-red.min.css">
	<script src="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.min.js"></script>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/blog-new.css') }}">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/global.css') }}">
	<script type="text/javascript" src="{{ asset('js/global.js') }}"></script>
	<script type='text/javascript' src='//eclkmpsa.com/adServe/banners?tid=59140_91736_2&tagid=2'></script>

</head>

<body>

    <nav id="menu" class="menu">
      <a href="http://livonair.com" target="_blank">
        <header class="menu-header">
          <span class="menu-header-title">Powered By LivonAir</span>
        </header>
      </a>

      <section class="menu-section">
        <h3 class="menu-section-title">About Blog</h3>
        <ul class="menu-section-list">
        	<li><a href="http://{{ $bloginfo['blogname'] }}.livonair.com">{{ $bloginfo['name'] }}</a></li>
          <li><a href="{{ url('live') }}" target="_blank">Live Blogs</a></li>
        </ul>
      </section>
	  <section class="menu-section">
        <h3 class="menu-section-title">LivonAir</h3>
        <ul class="menu-section-list">
        	@if(Auth::guest())
	          <li><a href="{{ url('login') }}" target="_blank">Login</a></li>
	          <li><a href="{{ url('register') }}" target="_blank">Register</a></li>
	        @else
			  <li><a href="{{ url('blogs') }}" target="_blank">My Blogs</a></li>
	        @endif

        </ul>
      </section>
    </nav>
<div id="panel">
<a href="#" class="toggle-button menu-btn close-transform"><span></span></a>
	<div class="mdl-layout mdl-js-layout">
      <div class="demo-ribbon"></div>
      <main class="demo-main mdl-layout__content">
        <div class="demo-container mdl-grid">
          <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
          <div class="demo-content mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col" itemprop='blogPost' itemscope='itemscope' itemtype='http://schema.org/BlogPosting'>
			@if($post['header_image_hash'] !== "none")
			<img src="https://s3-ap-southeast-1.amazonaws.com/livonair/header/{{ $post['header_image_hash'] }}" class="img-responsive" itemprop="image">
			@endif
            <div class="demo-crumbs mdl-color-text--grey-500">
              <time datetime="{{ $post['created_at'] }}">{{ date('F d, Y', strtotime($post['created_at'])) }}</time>
            </div>
            <h3>{{ $post['title'] }}</h3><hr/>
              <section class="link-textupline"><p>
              @if($post['file_type'] === 'MD')
              	{!! $converter->convertToHtml($post['post']) !!}
              @else
              	{!! $post['post'] !!}
              @endif
              </p></section><br/><br/>
              <hr/>

				<div class="row card">
					<div class="three columns">
						<img src="https://s3-ap-southeast-1.amazonaws.com/livonair/author/{{ $user['id'] }}" class="img-responsive img-circle img-thumbnail">
					</div>
					<div class="nine columns">
						<h3>{{ $user['name'] }}</h3>
						<p>{{ $user['info'] }}</p>
					</div>
				</div>

				<div class="social">

				</div>
						Tags: {{ $post['category'] }}
				<!-- START: Livefyre Embed -->
				<div id="livefyre-comments"></div>
				<script type="text/javascript" src="http://zor.livefyre.com/wjs/v3.0/javascripts/livefyre.js"></script>
				<script type="text/javascript">
				(function () {
				    var articleId = fyre.conv.load.makeArticleId(null);
				    fyre.conv.load({}, [{
				        el: 'livefyre-comments',
				        network: "livefyre.com",
				        siteId: "375927",
				        articleId: articleId,
				        signed: false,
				        collectionMeta: {
				            articleId: articleId,
				            url: fyre.conv.load.makeCollectionUrl(),
				        }
				    }], function() {});
				}());
				</script>
				<!-- END: Livefyre Embed -->

          </div>
        </div>
          	<footer class="mdl-mini-footer">
		  <div class="mdl-mini-footer--left-section">
		    <ul class="mdl-mini-footer--link-list">
		      <li>&copy; {{ $bloginfo['copyright'] or $bloginfo['name'] }}</li>
				@if(isset($bloginfo['fb']))
					<li><a class="fa fa-facebook-official" href="http://fb.com/{{ $bloginfo['fb'] }}"></a></li>
				@endif
				@if(isset($bloginfo['twitter']))
					<li><a class="fa fa-twitter" href="http://twitter.com/{{ $bloginfo['twitter'] }}"></a></li>
				@endif
				@if(isset($bloginfo['github']))
					<li><a class="fa fa-github" href="http://github.com/{{ $bloginfo['github'] }}"></a></li>
				@endif
				@if(isset($bloginfo['linkedin']))
					<li><a class="fa fa-linkedin" href="http://linkedin.com/{{ $bloginfo['linkedin'] }}"></a></li>
				@endif
				@if(isset($bloginfo['tumblr']))
					<li><a class="fa fa-tumblr" href="http://{{ $bloginfo['fb'] }}.tumblr.com"></a></li>
				@endif
				@if(isset($bloginfo['website']))
					<li><a class="fa fa-connectdevelop" href="http://{{ $bloginfo['website'] }}"></a></li>
				@endif
				@if(isset($bloginfo['email']))
					<li><a class="fa fa-envelope-o" href="mailto:{{ $bloginfo['email'] }}"></a></li>
				@endif
				@if(isset($bloginfo['telephone']))
					<li><a class="fa"> <i class="fa fa-phone"></i> {{ $bloginfo['telephone'] }} </a></li>
				@endif
<li><a href="{{ url('feed') }}" class="fa fa-rss"></a></li>
		      <li><a href="http://livonair.com">LivOnAir</a></li>
		    </ul>
		  </div>
		</footer>
      </main>

    </div>

	</div>
	<script type="text/javascript" src="{{ asset('/js/sweetalert.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/form.read.js') }}"></script>
	<!-- User ads -->
	@if(isset($bloginfo['analytics']))
			{!! $bloginfo['analytics'] !!}
	@endif
@if(\Auth::id() === $user['id'])
	<a class="fab mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect" href="{{ url('/post/' . $post['slug'] . '/delete') }}">
		<i class="material-icons">delete</i>
	</a>
	<a class="fab-admin mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect" href="{{ url('/post/' . $post['slug'] . '/edit') }}">
		<i class="material-icons">edit</i>
	</a>
@endif

	<script type="text/javascript">
	$(window).blur(function(){ document.title = "I'm waiting for you." });
	$(window).focus(function(){ document.title = "{{ $post['title'] }} | LivonAir" });
	</script>
		<script type="text/javascript">
		var slideout = new Slideout({
			'panel': document.getElementById("panel"),
			'menu': document.getElementById("menu"),
			'padding': 256,
			'tolerance': 70
		});

		document.querySelector('.toggle-button').addEventListener('click', function() {
	    	slideout.toggle();
	    });
	</script>

{{-- Google Analytics Code --}}
	<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-63818132-1', 'auto');ga('send', 'pageview');</script>
</body>
</html>

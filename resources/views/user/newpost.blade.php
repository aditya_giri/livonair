<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>New Post | LivOnAir</title>
	<link rel="stylesheet" href="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.indigo-pink.min.css">
	  	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" />
	<script src="https://storage.googleapis.com/code.getmdl.io/1.0.0/material.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	@if($bloginfo->editor_type === "MS")
		<script src="{{ asset('editor/ckeditor.js') }}"></script>
	@elseif($bloginfo->editor_type === "MD")
		<script type="text/javascript" src="{{ asset('md-editor/epiceditor.min.js') }}"></script>
		<style type="text/css">.editor{width: 100%;height:480px;}
		</style>
	@endif
	<script src="//fast.eager.io/ioo3Am8MmR.js"></script>
	<style type="text/css">
		.container {
			width: 80%;
			margin: 0 auto;
		}
	</style>
</head>
<body>

	<section class="container">
		@if(session()->has('edit'))
			<form method="post" data-remote enctype="multipart/form-data">
		@else
			<form method="post" action="{{ url('admin/new') }}" data-remote enctype="multipart/form-data">
		@endif
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			@if(session()->has('edit'))
				<input name="tags" class="mdl-textfield__input" type="text" id="sample3" value="{{ $post->category }}" />
			@else
				<input name="tags" class="mdl-textfield__input" type="text" id="sample3" />
			@endif
				<label class="mdl-textfield__label" for="sample3">Tags</label>
			</div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
			@if(\Session::has('edit'))
				<input name="title" class="mdl-textfield__input" type="text" required value="{{ $post->title }}"/>
			@else
				<input name="title" class="mdl-textfield__input" type="text" required />
			@endif
				<label class="mdl-textfield__label" for="sample3">Title</label>
			</div>
			<input type="hidden" name="editor_type" value="{{ $_GET['editor'] or $bloginfo->editor_type }}">
			@if($bloginfo->editor_type === "MS")
				<textarea name="editor" id="editor" rows="10" cols="100">
					@if(\Session::has('edit'))
						{!! $post->post !!}
					@else
						Hmmmmm. Really LivonAir is so cooooool
					@endif
				</textarea>
				<script>
					CKEDITOR.replace( 'editor' );
				</script>
				<script type="text/javascript" src="{{ asset('editor/config.js') }}"></script>
			@elseif($bloginfo->editor_type === "MD")
				<div id="epiceditor">
				</div>
				<input type="hidden" id="editor" name="editor">
				<script type="text/javascript">
					var opts = {
							file: {
								@if(\Session::has('edit'))
									defaultContent: '{!! $post->post !!}',
								@else
									defaultContent: '#### Hmmmmm. Really LivonAir is so cooooool',
								@endif
							},
							clientSideStorage: false,
							theme: {
								base: '{{ url("md-editor/themes/base/epiceditor.css") }}',
								preview: '{{ url("md-editor/themes/preview/preview-dark.css") }}',
								editor: '{{ url("md-editor/themes/editor/epic-dark.css") }}'
							},
						}
						var editor = new EpicEditor(opts).load();
						function getvalue(){
							var code = editor.getElement('editor').body.innerHTML;
							$('#editor').val(code);
					}
				</script>
			@endif
			<br/>
				Do you want to add a header Image? Click <input type="file" name="image" accept="image/*">
			<br/>
				<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" onclick="getvalue()">
					@if(session()->has('edit'))
					  Update
					@else
					  Create
					@endif
				</button>

		</form>
	</section>
	{{-- Google Analytics Code --}}
	<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-63818132-1', 'auto');ga('send', 'pageview');</script>
</body>
</html>

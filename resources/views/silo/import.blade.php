@extends('app')

@section('css')
	<style type="text/css">
		.card {
			width: 400px;
			margin: 0 auto;
			margin-top: 50px;
		}
	</style>

@stop

@section('title')
	Import from blog
@stop

@section('content')
	<div class="mdl-card mdl-shadow--2dp card">
	  <div class="mdl-card__title">
	    <h2 class="mdl-card__title-text">Import using RSS</h2>
	  </div>
	  <div class="mdl-card__supporting-text">
			<form role="form" method="POST">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<div class="form-group">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label textfield-demo">
					    <input class="mdl-textfield__input" id="url" type="url" name="url"/>
					    <label class="mdl-textfield__label" for="url">URL for <b>RSS</b></label>
					</div>
				</div>
				<hr/>
				<div class="form-group">
					<div class="col-md-6 col-md-offset-4">
						<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Start Import</button>
					</div>
				</div>
			</form>
	  </div>
	</div>

@stop

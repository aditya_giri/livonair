@extends('dashboard')

@section('title')
	Notifications
@stop

@section('css')
<style type="text/css">
	.blog-card {
		height: 50px;
	}
</style>
@stop

@section('content')
	@if (count($notifications) !== 0)

		@foreach ($notifications as $notification)
			@if($notification->read === 0)
				<div class="mdl-card mdl-shadow--2dp blog-card" id="{{ $notification->id }}">
				  <div class="mdl-card__supporting-text">
				    {!! $notification->notification !!}
				  </div>
				  <div class="mdl-card__actions mdl-card--border">
				    <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" onclick="read()">
				      Mark As Read
				    </a>
				  </div>
				</div>
				<script type="text/javascript">
					function read(){
						<?php $notification = \App\Notification::where('id', $notification->id)->first()?>
						<?php $notification->delete()?>
					}
				</script>
			@endif
		@endforeach

	@else
		No notifications for you.
	@endif
@stop

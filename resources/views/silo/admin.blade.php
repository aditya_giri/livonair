@if (Auth::guest() || Auth::id() !== $siloinfo['user_id'])
	<?php return \Redirect::to(url('/'))?>
@endif
<!DOCTYPE html>
<html>
<head>
	<title>Admin | LivOnAir</title>
	<meta charset="utf-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script src="//fast.eager.io/ioo3Am8MmR.js"></script>
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/flatly/bootstrap.min.css">
	<style type="text/css">
		.back-button {
			top: 30px;
			left: 0px;
			width: 50px;
			position: relative;
			padding: 10px;
			text-align: center;
			background-color: #1abc9c;
		}
		.back-button:hover {
			background-color: #16a085;
			-webkit-transition: all .25s ease;
			   -moz-transition: all .25s ease;
			    -ms-transition: all .25s ease;
			     -o-transition: all .25s ease;
			        transition: all .25s ease;
		}
		.back-button a {
			color: #f1c40f;
		}
		.container {
			padding: 10px;
			width: 60%;
			margin: auto;
		}
		#editor {
			height: 300px;
		}
		@media only screen and (max-width: 480px){
			.container {
				width: 100%;
			}
		}
		body > div
		{
	        transition:all 0.3s ease;
		}
	</style>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/ace.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/mode-css.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/snippets/css.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/worker-css.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/theme-terminal.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ace/1.1.9/ext-beautify.js"></script>
</head>
<body>
	<div class="back-button"><a href="{{ url('admin') }}" class="fa fa-backward"></a></div>
	<div class="container">
		@if (Session::has('status'))
			@if(Session::get('status') === "info")
				<div class="alert alert-success">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Success!</strong> Updated the changes. If you do not see, then refresh the page.
				</div>
			@elseif(Session::get('status') === "error")
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Sorry!</strong> Looks like something went wrong. Please try again.
				</div>
			@endif
		@endif
		<form method="POST" role="form" enctype="multipart/form-data">
			<legend>Edit Your Data</legend>
				<label for="siloname">Silo Name</label>
				<input class="form-control"  id="siloname" name="siloname" type="text" placeholder="{{ $siloinfo['blogname'] }}" disabled="">
			<br/>
				<label>Select Image</label>
				<input name="image" type="file" accept="image/*" >
				@if($siloinfo['image_name'] !== null)
					<img src="https://s3-ap-southeast-1.amazonaws.com/livonair/blog-profiles/{{ $siloinfo['image_name'] }}">
				@endif
			<br/>
				<label for="copyright">Copyright</label>
				<input type="text" class="form-control" name="copyright" id="copyright" value="{{ $siloinfo['copyright'] }}">
			<br />
				<label for="js">Your ads code:</label>
				<div class="js" id="editor">{{ $siloinfo['analytics'] }}</div>

				<div class="alert alert-info">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>Remember this</strong> Please enter correct analytics code. If you do not have one please get it from Google Analytics. We are currently working on our custom analytics reports. If you do not have one please leave it as it is. If we find any mischivous code, we may reject and even delete your site. Thank you for the support.
				</div>

				<input type="hidden" name="js" id="js">
			<div class="form-group">
				<label for="fb" class="control-label">Facebook Profile Page</label>
				<div class="input-group">
					<span class="input-group-addon" for="fb">facebook.com/</span>
					<input type="text" name="fb" id="fb" class="form-control" value="{{ $siloinfo['fb'] }}">
				</div>
			</div>
			<div class="form-group">
			  <label class="control-label">Twitter</label>
			  <div class="input-group">
			    <span class="input-group-addon" for="twitter">@</span>
			    <input class="form-control" type="text" id="twitter" name="twitter" value="{{ $siloinfo['twitter'] }}">
			  </div>
			</div>
			<div class="form-group">
				<label for="github" class="control-label">Github</label>
			<div class="input-group">
			    <span class="input-group-addon" for="github">github.com/</span>
			  	<input type="text" name="github" id="github" class="form-control" value="{{ $siloinfo['github'] }}">
			</div>
			</div>
			<div class="form-group">
				<label for="tumblr" class="control-label">Tumblr</label>
				<div class="input-group">
					<input type="text" name="tumblr" id="tumblr" class="form-control" value="{{ $siloinfo['tumblr'] }}">
					<span class="input-group-addon" for="tumblr">.tumblr.com</span>
				</div>
			</div>
			<div class="form-group">
				<label for="website" class="control-label">Website</label>
				<div class="input-group">
				<span class="input-group-addon" for="website">http://</span>
				<input type="text" name="website" id="website" class="form-control" value="{{ $siloinfo['website'] }}">
				</div>
			</div>
			<div class="form-group">
				<label for="linkedIn" class="control-label">LinkedIn</label>
				<div class="input-group">
					<span class="input-group-addon" for="linkedin">linkedin.com/</span>
					<input type="text" name="linkedin" id="linkedin" class="form-control" value="{{ $siloinfo['linkedin'] }}">
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="control-label">Email</label>
				<input type="email" name="email" id="email" class="form-control" value="{{ $siloinfo['email'] }}">
			</div>
			<div class="form-group">
				<label for="telephone" class="control-label">Telephone</label>
				<input type="tel" name="mobile" id="telephone" class="form-control" value="{{ $siloinfo['telephone'] }}">
			</div>
			<div class="form-group">
		      <label class="col-lg-2 control-label">Editor</label>
		      <div class="col-lg-10">
		        <div class="radio">
		          <label>
		            <input type="radio" name="editor_type" id="MS" value="MS" @if($siloinfo['editor_type'] === "MS") checked @endif>
		            MS Editor
		          </label>
		        </div>
		        <div class="radio">
		          <label>
		            <input type="radio" name="editor_type" id="MD" value="MD" @if($siloinfo['editor_type'] === "MD") checked @endif>
		            Markdown
		          </label>
		        </div>
		      </div>
		    </div>
		    <div class="form-group">
		      <label class="col-lg-2 control-label">Menu Type</label>
		      <div class="col-lg-10">
		        <div class="radio">
		          <label>
		            <input type="radio" name="menu" id="rotate" value="rotate" @if($siloinfo['editor_type'] === "rotate") checked @endif>
		            Rotate
		          </label>
		        </div>
		        <div class="radio">
		          <label>
		            <input type="radio" name="menu" id="dot-transform" value="dot-transform" @if($siloinfo['editor_type'] === "dot-transform") checked @endif>
		            Simple Dot Transform
		          </label>
		        </div>
		        <div class="radio">
		          <label>
		            <input type="radio" name="menu" id="rotate-and-line-transform" value="rotate-and-line-transform" @if($siloinfo['editor_type'] === "rotate-and-line-transform") checked @endif>
		            Rotate and Make a Line
		          </label>
		        </div>
		        <div class="radio">
		          <label>
		            <input type="radio" name="menu" id="close-transform" value="close-transform" @if($siloinfo['editor_type'] === "close-transform") checked @endif>
		            Close Transform
		          </label>
		        </div>
		        <div class="radio">
		          <label>
		            <input type="radio" name="menu" id="less-than-transform" value="less-than-transform" @if($siloinfo['editor_type'] === "less-than-transform") checked @endif>
		            Less than Transform
		          </label>
		        </div>
		        <div class="radio">
		          <label>
		            <input type="radio" name="menu" id="greater-than-transform" value="greater-than-transform" @if($siloinfo['editor_type'] === "greater-than-transform") checked @endif>
		            Greater than Transform
		          </label>
		        </div>
		        <div class="radio">
		          <label>
		            <input type="radio" name="menu" id="simple-dot-transform" value="simple-dot-transform" @if($siloinfo['editor_type'] === "simple-dot-transform") checked @endif>
		            Round Dot Transform
		          </label>
		        </div>
		        <div class="radio">
		          <label>
		            <input type="radio" name="menu" id="stair-case-transform" value="stair-case-transform" @if($siloinfo['editor_type'] === "stair-case-transform") checked @endif>
		            Stair Case Transform
		          </label>
		        </div>
		      </div>
		    </div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<button type="submit" class="btn btn-primary" onclick="getvalue()">Submit</button>
		</form>
	</div>
	<script type="text/javascript">
	    var editor = ace.edit("editor");
	    editor.setTheme("ace/theme/monokai");
	    editor.getSession().setMode("ace/mode/javascript");

	    function getvalue(){
	    	var code = editor.getValue();
	    	$('#js').val(code);
	    }
	</script>
</body>
</html>
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class UpdateBlogInfosTableSocialThings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog_info', function (Blueprint $table) {
            $table->string('fb');
            $table->string('twitter');
            $table->string('github');
            $table->string('tumblr');
            $table->string('website');
            $table->string('linkedin');
            $table->string('email');
            $table->string('telephone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

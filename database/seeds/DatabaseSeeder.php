<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$this->call('ScheduledAnalyticsUpdateTableSeeder');
		$this->command->info('Scheduled Seeding Completed');
	}

}

class ScheduledAnalyticsUpdateTableSeeder extends Seeder
{
    public function run()
    {
    	$blogs = \App\BlogInfo::all();
    	$date = new \Carbon\Carbon(); 
    	foreach ($blogs as $blog) {
    		$array[] = ['blogname' => $blog->blogname, 'pageviews' => '0', 'date' => date("Ymd")];
    	}

        DB::table('analytics')->insert($array);

    }
}
(function(){

	$('form[data-remote]').on('submit', function(e){
		
		e.preventDefault();

		var form = $(this);
		var method = form.find('input[name="_method"]').val() || 'POST';
		$.ajax({
			type: method,
			url: window.location.href,
			data: form.serialize(),
			success: function(r){$('#post').html(r.responseText},
			error: function(){swal("Error", "Looks like something went wrong.", "error")},
		});

	});


})();
(function(){

	$('form[data-remote]').on('submit', function(e){
		
		e.preventDefault();

		var form = $(this);
		var method = form.find('input[name="_method"]').val() || 'POST';
		var url = form.prop('action')
		$.ajax({
			type: method,
			url: url,
			data: form.serialize(),
			success: function(){swal("Success", "Created!", "success")},
			error: function(){swal("Error", "Sorry, looks like something went wrong. Please Check name of the blog or replace it as it may be in use.", "error")},
		});

	});


})();